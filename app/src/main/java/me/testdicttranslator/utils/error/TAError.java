package me.testdicttranslator.utils.error;

/**
 * Created by warabei on 12.02.2015.
 */
public enum TAError {
    EConnectionError,
    EJsonParsingError,
    EIOError,

    EDictionaryCorruptedError,
    EDictionaryReadingError,

    EDictionaryUnavailableError,
    EDictionaryLoadingError,
    EDictionaryLoadingFailedError,
    ENoDictionaryError,

}
