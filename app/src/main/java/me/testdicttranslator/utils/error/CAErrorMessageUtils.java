package me.testdicttranslator.utils.error;

import me.testdicttranslator.R;

/**
 * Created by warabei on 12.02.2015.
 */
public class CAErrorMessageUtils {

    public static int getErrorMessageId(TAError error){
        switch (error){
            case EConnectionError:
                return R.string.error_connection;
            case EJsonParsingError:
                return R.string.wrong_response;
            case EDictionaryCorruptedError:
                return R.string.dic_corrupted;
            case EDictionaryLoadingError:
                return R.string.dic_still_not_loaded_yes;
            case EDictionaryLoadingFailedError:
                return R.string.dic_load_failed;
            case EDictionaryReadingError:
                return R.string.dic_reading_failed;
            case EDictionaryUnavailableError:
                return R.string.dic_unavailable;
            case ENoDictionaryError:
                return R.string.dic_absent;

        }
        return 0;
    }
}
