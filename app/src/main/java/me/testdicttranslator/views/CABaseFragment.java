package me.testdicttranslator.views;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import me.testdicttranslator.managers.IAAppManager;
import me.testdicttranslator.managers.event.IAEvent;
import me.testdicttranslator.managers.event.IAEventDispatcher;
import me.testdicttranslator.managers.event.IAEventListener;

/**
 * Created by warabei on 12.02.2015.
 */
public class CABaseFragment extends Fragment implements IAEventListener {

    protected IAAppManager mAppManager;
    private IAEventDispatcher mEventDispatcher;
    private IAEvent.Type mEventType;
    private IAEvent.Type mEventTypesArray[];

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAppManager = ((CABaseActivity) getActivity()).getAppManager();
        mEventDispatcher = mAppManager.getEventDispatcher();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mEventDispatcher.subscribeListener(this, mEventType, mEventTypesArray);
    }

    @Override
    public void onPause() {
        super.onPause();
        mEventDispatcher.unsubscribeListener(this);
    }

    protected void addEvents(IAEvent.Type type, IAEvent.Type... types){
        mEventType = type;
        mEventTypesArray = types;
    }

    @Override
    public void onNewEvent(IAEvent event) {
    }
}
