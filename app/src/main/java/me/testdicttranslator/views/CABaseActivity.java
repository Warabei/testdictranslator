package me.testdicttranslator.views;

import android.app.Activity;
import android.os.Bundle;
import me.testdicttranslator.CABaseApplication;
import me.testdicttranslator.managers.IAAppManager;

/**
 * Created by warabei on 12.02.2015.
 */
public class CABaseActivity extends Activity{
    protected IAAppManager mAppManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppManager = ((CABaseApplication) getApplication()).getAppManager();
    }

    public IAAppManager getAppManager(){
        return mAppManager;
    }
}
