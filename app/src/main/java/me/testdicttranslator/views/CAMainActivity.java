package me.testdicttranslator.views;

import android.os.Bundle;
import me.testdicttranslator.R;


public class CAMainActivity extends CABaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_main);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new CAMainFragment())
					.commit();
		}
	}

}
