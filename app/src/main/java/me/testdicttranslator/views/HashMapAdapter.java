package me.testdicttranslator.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by warabei on 15.02.2015.
 */
public class HashMapAdapter extends BaseAdapter {

    private final List <Map.Entry<String, String>> mData;
	private final boolean mIsReversed;

	public HashMapAdapter(Map<String, String> map, boolean mIsReversed) {
		this.mIsReversed = mIsReversed;
		mData = new ArrayList<Map.Entry<String, String>>(map.size());
		mData.addAll(map.entrySet());
	}

	@Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, String> getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

		ViewHolder viewHolder;
        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.textView1 = (TextView) result.findViewById(android.R.id.text1);
			viewHolder.textView2 = (TextView) result.findViewById(android.R.id.text2);
			result.setTag(viewHolder);
        } else {
            result = convertView;
			viewHolder = (ViewHolder) result.getTag();
        }

        Map.Entry<String, String> item = getItem(position);

		if (mIsReversed){
			viewHolder.textView1.setText(String.format("Source: %s", item.getValue()));
			viewHolder.textView2.setText(String.format("Translate: %s", item.getKey()));
		} else {
			viewHolder.textView1.setText(String.format("Source: %s", item.getKey()));
			viewHolder.textView2.setText(String.format("Translate: %s", item.getValue()));
		}

        return result;
    }

	private static class ViewHolder {
		TextView textView1;
		TextView textView2;
	}
}
