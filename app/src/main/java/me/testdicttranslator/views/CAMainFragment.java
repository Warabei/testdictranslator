package me.testdicttranslator.views;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.*;
import android.widget.*;
import me.testdicttranslator.R;
import me.testdicttranslator.managers.IADictionaryManager;
import me.testdicttranslator.managers.event.IAEvent;
import me.testdicttranslator.managers.IATranslateManager;
import me.testdicttranslator.utils.error.CAErrorMessageUtils;
import me.testdicttranslator.utils.error.TAError;

import java.util.Map;

/**
 * A placeholder fragment containing a simple view.
 */
public class CAMainFragment extends CABaseFragment implements View.OnClickListener {

    private EditText mToTranslateEditText;
    private TextView mTranslatedTextView;
    private IATranslateManager mTranslateManager;
	private ListView mDictionaryListView;
    private IADictionaryManager mDictionaryManager;
	private String mSentence;
	private String mTranslated;
	private Button mToDic;
	private EditText mDictionaryFilter;
	private ProgressBar mProgressBar;
	private ViewGroup mContent;
	private boolean mIsReversedDictionary;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addEvents(
                IAEvent.Type.ETranslationResults,
                IAEvent.Type.EDictionaryAvailability,
                IAEvent.Type.EDictionaryPrefixed,
                IAEvent.Type.EDictionaryReversedPrefixed,
				IAEvent.Type.EDictionaryWordAddedSuccessfully,
				IAEvent.Type.EDictionaryWordAddedInMemory,
				IAEvent.Type.EDictionaryWordAlreadyExists
        );
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.f_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.submit).setOnClickListener(this);
		mToDic = (Button) view.findViewById(R.id.to_dic);
		mToDic.setOnClickListener(this);
		mProgressBar = (ProgressBar) view.findViewById(R.id.progress_wheel);
		mContent = (ViewGroup) view.findViewById(R.id.content_list_area);
		mToTranslateEditText = (EditText) view.findViewById(R.id.to_translate);
        mTranslatedTextView = (TextView) view.findViewById(R.id.translated);
        mTranslatedTextView.setMovementMethod(new ScrollingMovementMethod());
        mDictionaryListView = (ListView) view.findViewById(R.id.dictionary);
        setupDictionaryFilter(view);
		setupLangSpinner(view);
        mTranslateManager = mAppManager.getTranslateManager();
        mDictionaryManager = mAppManager.getDictionaryManager();
    }

	private void setupLangSpinner(View view) {
		Spinner mLangSpinner = (Spinner) view.findViewById(R.id.lang_spinner);
		mLangSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mIsReversedDictionary = position != 0; //ru vs eng
				if (mDictionaryManager.isLoaded()) {
					search(mDictionaryFilter.getText().toString());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	private void setupDictionaryFilter(View view) {
		mDictionaryFilter = (EditText) view.findViewById(R.id.filter);
		mDictionaryFilter.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (mDictionaryManager.isLoaded()){
					search(mDictionaryFilter.getText().toString());
				}
			}
		});
	}

	private void search(String key) {
		if (mIsReversedDictionary) {
			mDictionaryManager.requestReversedDictionaryStartedWithPrefix(key);
		} else {
			mDictionaryManager.requestDictionaryStartedWithPrefix(key);
		}
	}

	@Override
    public void onResume() {
        super.onResume();
        mTranslateManager.requestLastTranslation();
		if (mDictionaryManager.isLoading()){
			hideContent();
		} else if (mDictionaryManager.isLoaded()){
			showContent();
		} else {
			hideAll();
		}
    }

	private void hideAll() {
		mContent.setVisibility(View.GONE);
		mProgressBar.setVisibility(View.GONE);
	}

	private void hideContent() {
		mProgressBar.setVisibility(View.VISIBLE);
		mContent.setVisibility(View.GONE);
	}

	@Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                mTranslateManager.translate(mToTranslateEditText.getText().toString());
                break;
			case R.id.to_dic:
				mDictionaryManager.saveWord(mSentence, mTranslated);
				break;
        }
    }

    @Override
    public void onNewEvent(IAEvent event) {
        super.onNewEvent(event);
        switch (event.getType()){
            case ETranslationResults:
                onTranslationResultObtained(event);
                break;
            case EDictionaryAvailability:
                TAError error = event.getObject(IAEvent.ERROR);
                if (error == null) {
					showDictionary();
                } else {
                    Toast.makeText(getActivity(), CAErrorMessageUtils.getErrorMessageId(error), Toast.LENGTH_LONG).show();
                }
                break;
            case EDictionaryPrefixed:
                Map<String, String> dictionary = event.getObject(IAEvent.DICTIONARY_PREFIXED);
                setAdapter(dictionary);
                break;
            case EDictionaryReversedPrefixed:
                Map<String, String> reversedDictionary = event.getObject(IAEvent.DICTIONARY_REVERSED_PREFIXED);
                setAdapter(reversedDictionary);
                break;
			case EDictionaryWordAddedSuccessfully:
				showContent();
				search(mDictionaryFilter.getText().toString());
				Toast.makeText(getActivity(), R.string.word_added_success, Toast.LENGTH_SHORT).show();
				break;
			case EDictionaryWordAddedInMemory:
				showContent();
				search(mDictionaryFilter.getText().toString());
				Toast.makeText(getActivity(), R.string.word_added_in_memory, Toast.LENGTH_SHORT).show();
				break;
			case EDictionaryWordAlreadyExists:
				Toast.makeText(getActivity(), R.string.word_exists, Toast.LENGTH_SHORT).show();
				break;
        }
    }

	private void showDictionary() {
		showContent();
		mDictionaryManager.requestDictionaryStartedWithPrefix("");
	}

	private void showContent() {
		mContent.setVisibility(View.VISIBLE);
		mProgressBar.setVisibility(View.GONE);
	}

	private void setAdapter(Map<String, String> dictionary) {
        HashMapAdapter adapter = new HashMapAdapter(dictionary, mIsReversedDictionary);
        mDictionaryListView.setAdapter(adapter);
    }

    private void onTranslationResultObtained(IAEvent event) {
        TAError error = event.getObject(IAEvent.ERROR);
        if (error == null) {
			mSentence = event.getString(IAEvent.TRANSLATION_RESULTS_SENTENCE);
			mToTranslateEditText.setText(mSentence);
			mToTranslateEditText.setSelection(mSentence.length());
			mTranslated = event.getString(IAEvent.TRANSLATION_RESULTS_TRANSLATION);
			mTranslatedTextView.setText(mTranslated);
			mToDic.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getActivity(), CAErrorMessageUtils.getErrorMessageId(error), Toast.LENGTH_LONG).show();
        }
    }

}
