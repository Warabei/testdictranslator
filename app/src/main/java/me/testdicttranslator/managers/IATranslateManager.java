package me.testdicttranslator.managers;

import me.testdicttranslator.managers.IAManager;

/**
 * Created by warabei on 11.02.2015.
 */
public interface IATranslateManager extends IAManager{

    public void translate(String sentence);

    void requestLastTranslation();
}
