package me.testdicttranslator.managers;

import me.testdicttranslator.managers.IAManager;

/**
 * Created by warabei on 15.02.2015.
 */
public interface IADictionaryManager extends IAManager{
    //может быть медленно, в реальной жизни надо ограничивать выдачу
    void requestDictionaryStartedWithPrefix(String prefix);

    //может быть медленно, в реальной жизни надо ограничивать выдачу
    void requestReversedDictionaryStartedWithPrefix(String prefix);

	void saveWord(String key, String value);

	boolean isLoading();

	boolean isLoaded();
}
