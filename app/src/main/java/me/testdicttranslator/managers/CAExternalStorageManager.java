package me.testdicttranslator.managers;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by warabei on 15.02.2015.
 */
public class CAExternalStorageManager implements IAExternalStorageManager {

    private final Context mContext;

    public CAExternalStorageManager(Context context) {
        this.mContext = context;
    }

    @Override
    public boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    @Override
    public File getFile(String fileName) {
		return new File(mContext.getExternalFilesDir(
			   null), fileName);
    }

    @Override
    public void start() {

    }
}
