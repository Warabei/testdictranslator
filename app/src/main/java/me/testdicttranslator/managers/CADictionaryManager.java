package me.testdicttranslator.managers;

import android.os.Process;
import android.text.TextUtils;
import me.testdicttranslator.managers.event.CAEvent;
import me.testdicttranslator.managers.event.IAEvent;
import me.testdicttranslator.managers.event.IAEventDispatcher;
import me.testdicttranslator.managers.event.IAEventListener;
import me.testdicttranslator.utils.error.TAError;
import org.apache.commons.collections4.trie.PatriciaTrie;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.Collections;
import java.util.Map;

/**
 * Created by warabei on 15.02.2015.
 */
public class CADictionaryManager implements IADictionaryManager, IAEventListener {

	//со стейтами нужно поаккуратнее, но это тестовое же
    public enum State {NOT_INITED, LOADING, LOADED, LOADING_FAILED, NO_DICTIONARY, UNAVAILABLE}

    private final static String DICTIONARY_FILE_NAME = "dictionary";
    private final IAEventDispatcher mEventDispatcher;
    private final IAExternalStorageManager mExternalStorageManager;
    private State mState;

	//RadixTree куда, просто почти на порядок быстрее любого sqlite с fts
    private PatriciaTrie<String> mDictionary = new PatriciaTrie<String>();
    private PatriciaTrie<String> mReversedDictionary = new PatriciaTrie<String>();


    //по идее можно слушать когда флешку подключают
    public CADictionaryManager(IAEventDispatcher eventDispatcher, IAExternalStorageManager externalStorageManager) {
        this.mEventDispatcher = eventDispatcher;
        this.mExternalStorageManager = externalStorageManager;
        this.mState = State.NOT_INITED;
    }

    @Override
    public void start(){
        mEventDispatcher.subscribeListener(this, IAEvent.Type.EDictionaryLoadResults);
        loadDictionary();
    }

    private void loadDictionary(){
        if (!mExternalStorageManager.isExternalStorageAvailable()){
            mState = State.UNAVAILABLE;
			sendErrorEvent(TAError.EDictionaryUnavailableError);
            return;
        }

        final File dictionaryFile = mExternalStorageManager.getFile(DICTIONARY_FILE_NAME);
        if (!dictionaryFile.exists()){
            mState = State.NO_DICTIONARY;
			sendErrorEvent(TAError.ENoDictionaryError);
            return;
        }

        mState = State.LOADING;
        new Thread(new LoadDictionaryTask(mEventDispatcher, dictionaryFile)).start();
    }

    //может быть медленно, в реальной жизни надо ограничивать выдачу
    @Override
    public void requestDictionaryStartedWithPrefix(String prefix){
        if (checkState()){
            final Map<String, String> prefixDictionary;
            if (TextUtils.isEmpty(prefix)){
                prefixDictionary = Collections.unmodifiableMap(mDictionary);
            } else {
                prefixDictionary = Collections.unmodifiableMap(mDictionary.prefixMap(prefix));
            }
            sendPrefixedDictionaryEvent(prefixDictionary);
        }
    }

    //может быть медленно, в реальной жизни надо ограничивать выдачу
    @Override
    public void requestReversedDictionaryStartedWithPrefix(String prefix) {
        if (checkState()){
            final Map<String, String> prefixReversedDictionary;
            if (TextUtils.isEmpty(prefix)){
                prefixReversedDictionary = Collections.unmodifiableMap(mReversedDictionary);
            } else {
                prefixReversedDictionary = Collections.unmodifiableMap(mReversedDictionary.prefixMap(prefix));
            }
            sendPrefixedReversedDictionaryEvent(prefixReversedDictionary);
        }
    }

    private void sendPrefixedDictionaryEvent(Map <String, String> dictionary) {
        IAEvent event = new CAEvent(IAEvent.Type.EDictionaryPrefixed);
        event.put(IAEvent.DICTIONARY_PREFIXED, dictionary);
        mEventDispatcher.sendEvent(event);
    }

    private void sendPrefixedReversedDictionaryEvent(Map<String, String> reversedDictionary) {
        IAEvent event = new CAEvent(IAEvent.Type.EDictionaryReversedPrefixed);
        event.put(IAEvent.DICTIONARY_REVERSED_PREFIXED, reversedDictionary);
        mEventDispatcher.sendEvent(event);
    }

    private boolean checkState() {
        switch (mState){
            case LOADED:
                return true;
            case LOADING:
                sendErrorEvent(TAError.EDictionaryLoadingError);
                return false;
            case LOADING_FAILED:
                sendErrorEvent(TAError.EDictionaryLoadingFailedError);
                return false;
            case NO_DICTIONARY:
                sendErrorEvent(TAError.ENoDictionaryError);
                return false;
            case UNAVAILABLE:
                sendErrorEvent(TAError.EDictionaryUnavailableError);
                return false;
            case NOT_INITED:
                return false;
            default:
                return false;
        }
    }

	@Override
	public void saveWord(String key, String value){
		key = key.trim().toLowerCase();
		value = value.trim().toLowerCase();
		if (key.equals("") || value.equals(""))
			return;
		switch (mState){
			case LOADED:
			case NO_DICTIONARY:
				if (addToInMemoryDictionaryWithConfirmation(mDictionary, mReversedDictionary, key, value)){
					if (writeToDictionaryFile(key, value)){
						mEventDispatcher.sendEvent(new CAEvent(IAEvent.Type.EDictionaryWordAddedSuccessfully));
					} else {
						mEventDispatcher.sendEvent(new CAEvent(IAEvent.Type.EDictionaryWordAddedSuccessfully));
					}
				} else {
					mEventDispatcher.sendEvent(new CAEvent(IAEvent.Type.EDictionaryWordAlreadyExists));
				}
				mState = State.LOADED;
				break;
		}
	}

	@Override
	public boolean isLoading() {
		return mState == State.LOADING;
	}

	@Override
	public boolean isLoaded() {
		return mState == State.LOADED;
	}

	private boolean writeToDictionaryFile(String key, String value) {
		final File dictionaryFile = mExternalStorageManager.getFile(DICTIONARY_FILE_NAME);
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(new BufferedWriter(new FileWriter(dictionaryFile, true)));
			printWriter.printf("{\"key\":\"%s\",\"value\":\"%s\"}%n", key, value);
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			if (printWriter != null)
				printWriter.close();
		}
	}

	private static boolean addToInMemoryDictionaryWithConfirmation(PatriciaTrie<String> dictionary,
																   PatriciaTrie<String> reversedDictionary, String key, String value) {
		String existedValue = dictionary.get(key);
		if (existedValue != null) {
			return false;
		} else {
			addToInMemoryDictionary(dictionary, reversedDictionary, key, value);
			return true;
		}
	}

	private static void addToInMemoryDictionary(PatriciaTrie<String> dictionary,
												PatriciaTrie<String> reversedDictionary, String key, String value) {
		dictionary.put(key, value);
		String reversedValue = reversedDictionary.get(value);
		if (reversedValue != null) {
			reversedDictionary.put(value, reversedValue + ", " + key);
		} else {
			reversedDictionary.put(value, key);
		}
	}

	private void sendErrorEvent(TAError error) {
        IAEvent event = new CAEvent(IAEvent.Type.EDictionaryAvailability);
        event.put(IAEvent.ERROR, error);
        mEventDispatcher.sendEvent(event);
    }

    @Override
    public void onNewEvent(IAEvent event) {
        switch (event.getType()){
            case EDictionaryLoadResults:
                TAError taError = event.getObject(IAEvent.ERROR);
                if (taError != null) {
                    mState = State.LOADING_FAILED;
                    switch (taError){
                        case EJsonParsingError:
                            mEventDispatcher.sendEvent(buildDictionaryAvailableFailedEvent(TAError.EDictionaryCorruptedError));
                        case EIOError:
                            mEventDispatcher.sendEvent(buildDictionaryAvailableFailedEvent(TAError.EDictionaryReadingError));
                    }
                } else {
                    mDictionary = event.getObject(IAEvent.DICTIONARY_LOAD_RESULTS_DICTIONARY);
                    mReversedDictionary = event.getObject(IAEvent.DICTIONARY_LOAD_RESULTS_REVERSED_DICTIONARY);
                    mState = State.LOADED;
                    mEventDispatcher.sendEvent(new CAEvent(IAEvent.Type.EDictionaryAvailability));
                }
                break;
        }

    }

    private IAEvent buildDictionaryAvailableFailedEvent(TAError error) {
        IAEvent event = new CAEvent(IAEvent.Type.EDictionaryLoadResults);
        event.put(IAEvent.ERROR, error);
        return event;
    }


    private static class LoadDictionaryTask implements Runnable{

        private final IAEventDispatcher mEventDispatcher;
        private final File mDictionaryFile;

        public LoadDictionaryTask(IAEventDispatcher eventDispatcher, File dictionaryFile) {
            this.mEventDispatcher = eventDispatcher;
            this.mDictionaryFile = dictionaryFile;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            loadDictionary();
        }

        private void loadDictionary() {
            PatriciaTrie<String> dictionary = new PatriciaTrie<String>();
            PatriciaTrie<String> reversedDictionary = new PatriciaTrie<String>();
            try {
                loadWords(dictionary, reversedDictionary);
                mEventDispatcher.sendEvent(buildDictionaryLoadedEvent(dictionary, reversedDictionary));
            } catch (IOException e) {
                mEventDispatcher.sendEvent(buildDictionaryLoadedFailedEvent(TAError.EIOError));
            } catch (JSONException e) {
                mEventDispatcher.sendEvent(buildDictionaryLoadedFailedEvent(TAError.EJsonParsingError));
            }
        }

        private IAEvent buildDictionaryLoadedEvent(PatriciaTrie<String> dictionary, PatriciaTrie<String> reversedDictionary) {
            IAEvent event = new CAEvent(IAEvent.Type.EDictionaryLoadResults);
            event.put(IAEvent.DICTIONARY_LOAD_RESULTS_DICTIONARY, dictionary);
            event.put(IAEvent.DICTIONARY_LOAD_RESULTS_REVERSED_DICTIONARY, reversedDictionary);
            return event;
        }

        private IAEvent buildDictionaryLoadedFailedEvent(TAError error) {
            IAEvent event = new CAEvent(IAEvent.Type.EDictionaryLoadResults);
            event.put(IAEvent.ERROR, error);
            return event;
        }


        private void loadWords(PatriciaTrie<String> dictionary, PatriciaTrie<String> reversedDictionary)
                throws IOException, JSONException {
            BufferedReader reader = null;
            final String KEY = "key";
            final String VALUE = "value";
            try {
                InputStream inputStream = new FileInputStream(mDictionaryFile);
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                JSONObject jsonObject;
                String key;
                String value;
                while ((line = reader.readLine()) != null) {
                    jsonObject = new JSONObject(line);
                    key = jsonObject.optString(KEY);
                    value = jsonObject.optString(VALUE);
                    addToInMemoryDictionary(dictionary, reversedDictionary, key, value);
                }
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException ignored) {}
            }
        }
    }


}
