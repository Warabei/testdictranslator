package me.testdicttranslator.managers;

import me.testdicttranslator.managers.event.CAEvent;
import me.testdicttranslator.managers.event.IAEvent;
import me.testdicttranslator.managers.event.IAEventDispatcher;
import me.testdicttranslator.utils.error.TAError;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CAHttpsManager implements IAHttpsManager {

    private HttpClient mHttpClient;
    private final IAEventDispatcher mEventDispatcher;
    private WeakReference<HttpTask> mCurrentTask;

    public CAHttpsManager(IAEventDispatcher eventDispatcher) {
        mEventDispatcher = eventDispatcher;
    }


    private static HttpPost getPost(String url, Map<String, String> params) {
        HttpPost post = new HttpPost(url);
        if (params != null) {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            for (String key : params.keySet()) {
                nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
            }
            try {
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            } catch (UnsupportedEncodingException ignored) {}
        }
        return post;
    }


    @Override
    public void requestStringData(final String url, final Map<String, String> params){
        HttpTask httpTask = new HttpTask(url, params, mHttpClient, mEventDispatcher);
        abortCurrentTask();
        registerNewTask(httpTask);
        new Thread(httpTask).start();
    }

    private void registerNewTask(HttpTask httpTask) {
        mCurrentTask = new WeakReference<HttpTask>(httpTask);
    }

	//удобно отменять текущее задание, чтобы зависший хттп-запрос не висел
	//мы же помним, что оно синхронное?
    private void abortCurrentTask() {
        if (mCurrentTask != null) {
            HttpTask httpTask = mCurrentTask.get();
            if (httpTask != null){
                httpTask.abortTask();
            }
        }
    }


    private static String parseResponse(HttpResponse response) throws IOException {
        return EntityUtils.toString(response.getEntity());
    }

    private static boolean isResponseOk(HttpResponse response) {
        return response.getStatusLine().getStatusCode() == 200;
    }

    @Override
    public void start() {
        initHttpClient();
    }

    private void initHttpClient() {
        final HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);


        final SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

        final ClientConnectionManager clientConnManager = new SingleClientConnManager(httpParameters, schemeRegistry);
        mHttpClient = new DefaultHttpClient(clientConnManager, httpParameters);
    }

    private static class HttpTask implements Runnable{

        private final HttpClient mHttpClient;
        private final IAEventDispatcher mEventDispatcher;
        private final Map<String, String> mParams;
        private final HttpPost mPost;

        public HttpTask(String url, Map<String, String> params, HttpClient httpClient, IAEventDispatcher eventDispatcher) {
            this.mHttpClient = httpClient;
            this.mEventDispatcher = eventDispatcher;
            this.mPost = getPost(url, params);
            this.mParams = params;
        }

        void abortTask(){
            mPost.abort();
        }

        @Override
        public void run() {
            try {
                HttpResponse response = mHttpClient.execute(mPost);
                if (isResponseOk(response)){
                    String responseString = parseResponse(response);
                    IAEvent event = new CAEvent(IAEvent.Type.EStringResponse);
                    event.put(IAEvent.STRING_RESPONSE_MESSAGE, responseString);
                    event.put(IAEvent.STRING_RESPONSE_PARAMS, mParams);
                    mEventDispatcher.sendEvent(event);
                }
            } catch (IOException e) {
                if (!mPost.isAborted()){
                    IAEvent event = new CAEvent(IAEvent.Type.EStringResponse);
                    event.put(IAEvent.ERROR, TAError.EConnectionError);
                    mEventDispatcher.sendEvent(event);
                }
            }
        }
    }

}
