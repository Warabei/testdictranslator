package me.testdicttranslator.managers;

import java.util.Map;

/**
 * Created by warabei on 11.02.2015.
 */
public interface IAHttpsManager extends IAManager {
    void requestStringData(String url, Map<String, String> params);
}
