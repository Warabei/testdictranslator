package me.testdicttranslator.managers.event;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by warabei on 11.02.2015.
 */
public class CAEvent implements IAEvent {

    private final Map<String, Object> mMap;
    private final IAEvent.Type mType;

    public CAEvent(Type type) {
        this.mType = type;
        mMap = new HashMap<String, Object>();
    }

    @Override
    public Type getType() {
        return mType;
    }

    @Override
    public void put(String key, Object value) {
        mMap.put(key, value);
    }

    @Override
    public String getString(String key){
        return getObject(key);
    }

    @Override
    public boolean getBoolean(String key, boolean defaultValue) {
        Boolean bool = getObject(key);
        return bool != null ? bool : defaultValue;
    }

    /*public int getInt(String key){
        Object o = mMap.get(key);
        if (o == null) {
            return defaultValue;
        }
        try {
            return (Integer) o;
        } catch (ClassCastException e) {
            typeWarning(key, o, "Integer", defaultValue, e);
            return defaultValue;
        }
    }*/

    @Override
    public <T> T getObject(String key) {
        return (T) mMap.get(key);
    }
}
