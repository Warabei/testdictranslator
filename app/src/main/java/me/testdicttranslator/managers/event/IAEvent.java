package me.testdicttranslator.managers.event;

import android.os.Bundle;

/**
 * Created by warabei on 11.02.2015.
 */
public interface IAEvent {
    Type getType();

    String ERROR = "ERROR";

    String STRING_RESPONSE_MESSAGE = "STRING_RESPONSE_MESSAGE";
    String STRING_RESPONSE_PARAMS = "STRING_RESPONSE_PARAMS";

    String TRANSLATION_RESULTS_SENTENCE = "TRANSLATION_RESULTS_SENTENCE";
    String TRANSLATION_RESULTS_TRANSLATION = "TRANSLATION_RESULTS_TRANSLATION";

    String DICTIONARY_LOAD_RESULTS_DICTIONARY = "DICTIONARY_LOAD_RESULTS_DICTIONARY";
    String DICTIONARY_LOAD_RESULTS_REVERSED_DICTIONARY = "DICTIONARY_LOAD_RESULTS_REVERSED_DICTIONARY";

    String DICTIONARY_PREFIXED = "DICTIONARY_PREFIXED";
    String DICTIONARY_REVERSED_PREFIXED = "DICTIONARY_REVERSED_PREFIXED";

    void put(String key, Object value);

    String getString(String key);
    boolean getBoolean(String key, boolean defaultValue);

    <T> T getObject(String key);

    public enum Type {
        EStringResponse,
        ETranslationResults,
        EDictionaryLoadResults,
        EDictionaryAvailability,
        EDictionaryPrefixed,
        EDictionaryReversedPrefixed,

		EDictionaryWordAddedSuccessfully,
		EDictionaryWordAddedInMemory,
		EDictionaryWordAlreadyExists,
    }


}
