package me.testdicttranslator.managers.event;

import android.os.Handler;
import android.util.Log;

import java.util.*;


/**
 * Created by warabei on 11.02.2015.
 */
public class CAEventDispatcher implements IAEventDispatcher {

    private final HashMap<IAEventListener, Set<IAEvent.Type>> mListenersMap;
    private final Handler mMainThreadHandler;

    public CAEventDispatcher(Handler handler) {
        mMainThreadHandler = handler;
        mListenersMap = new HashMap<IAEventListener, Set<IAEvent.Type>>();
    }

    @Override
    public void subscribeListener(IAEventListener eventListener, IAEvent.Type type, IAEvent.Type... types) {
        Set<IAEvent.Type> eventListenerSet = mListenersMap.get(eventListener);

        if (eventListenerSet == null){
            eventListenerSet = EnumSet.of(type, types);
            mListenersMap.put(eventListener, eventListenerSet);
        }
        Collections.addAll(eventListenerSet, types);
    }

    @Override
    public void unsubscribeListener(IAEventListener eventListener) {
        mListenersMap.remove(eventListener);
    }

    @Override
    public void sendEvent(final IAEvent event) {
        mMainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                IAEvent.Type type = event.getType();
                for (Map.Entry<IAEventListener, Set<IAEvent.Type>> entry : mListenersMap.entrySet()){
                    if (entry.getValue().contains(type)){
                        entry.getKey().onNewEvent(event);
                    }
                }
            }
        });
    }
}
