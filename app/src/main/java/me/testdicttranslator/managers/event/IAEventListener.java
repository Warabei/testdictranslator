package me.testdicttranslator.managers.event;

/**
 * Created by warabei on 11.02.2015.
 */
public interface IAEventListener {
    void onNewEvent(IAEvent event);
}
