package me.testdicttranslator.managers.event;

/**
 * Created by warabei on 11.02.2015.
 */
public interface IAEventDispatcher {

    void subscribeListener(IAEventListener eventListener, IAEvent.Type type,IAEvent.Type... types);
    void unsubscribeListener(IAEventListener eventListener);
    void sendEvent(IAEvent event);
}
