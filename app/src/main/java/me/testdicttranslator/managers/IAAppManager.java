package me.testdicttranslator.managers;

import me.testdicttranslator.managers.event.IAEventDispatcher;

/**
 * Created by warabei on 11.02.2015.
 */
public interface IAAppManager extends IAManager{
    IAEventDispatcher getEventDispatcher();

    IATranslateManager getTranslateManager();

    IADictionaryManager getDictionaryManager();
}
