package me.testdicttranslator.managers;

/**
 * Created by warabei on 15.02.2015.
 */
public interface IAManager {
    void start();
}
