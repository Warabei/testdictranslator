package me.testdicttranslator.managers;

import me.testdicttranslator.managers.event.CAEvent;
import me.testdicttranslator.managers.event.IAEvent;
import me.testdicttranslator.managers.event.IAEventDispatcher;
import me.testdicttranslator.managers.event.IAEventListener;
import me.testdicttranslator.utils.error.TAError;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by warabei on 11.02.2015.
 */
public class CATranslateManager implements IATranslateManager, IAEventListener {

    private final static String TRANSLATE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    private final static String API_KEY = "trnsl.1.1.20150211T131137Z.9d6fe76ec3ec10fe.80e244b6a1482fdea0548e99b3819c0400dc687e";
    private final static String LANGUAGE = "en-ru";
    private final static String PARAM_KEY = "key";
    private final static String PARAM_LANG = "lang";
    private final static String PARAM_TEXT = "text";

    private final IAHttpsManager mHttpsManager;
    private final IAEventDispatcher mEventDispatcher;
    private String mCachedSentence;
    private String mCachedTranslatedString;

	//cюда тоже по идее можно стейты добавить
    public CATranslateManager(IAHttpsManager httpsManager, IAEventDispatcher eventDispatcher) {
        this.mHttpsManager = httpsManager;
        this.mEventDispatcher = eventDispatcher;
    }

    @Override
    public void translate(String sentence) {
        mHttpsManager.requestStringData(TRANSLATE_URL, buildParamsMap(sentence));
    }

    @Override
    public void requestLastTranslation() {
        if (mCachedSentence != null){
            mEventDispatcher.sendEvent(buildTranslationEvent(mCachedSentence, mCachedTranslatedString));
        }
    }

    @Override
    public void onNewEvent(IAEvent event) {
        switch (event.getType()){
            case EStringResponse:
                cleanCache();
                parseStringResponse(event);
                break;
        }
    }

    private void cleanCache() {
        mCachedSentence = null;
        mCachedTranslatedString = null;
    }

    private void cache(String sentence, String translatedString) {
        mCachedSentence = sentence;
        mCachedTranslatedString = translatedString;
    }

    private Map<String, String> buildParamsMap(String sentence){
        HashMap<String, String> params = new HashMap<String, String>(3);
        params.put(PARAM_KEY, API_KEY);
        params.put(PARAM_LANG, LANGUAGE);
        params.put(PARAM_TEXT, sentence);
        return params;
    }

    private void parseStringResponse(IAEvent event) {

        TAError error = event.getObject(IAEvent.ERROR);

        if (error == null){
            Map <String, String> paramsMap = event.getObject(IAEvent.STRING_RESPONSE_PARAMS);
            String sentence = paramsMap.get(PARAM_TEXT);
            String jsonString = event.getString(IAEvent.STRING_RESPONSE_MESSAGE);
            String translatedString = getTranslationFromJson(jsonString);
            if (translatedString != null){
                cache(sentence, translatedString);
                mEventDispatcher.sendEvent(buildTranslationEvent(sentence, translatedString));
            } else {
                mEventDispatcher.sendEvent(buildErrorTranslationEvent(TAError.EJsonParsingError));
            }
        } else {
            mEventDispatcher.sendEvent(buildErrorTranslationEvent(error));
        }
    }



    private IAEvent buildTranslationEvent(String sentence, String translatedString) {
        IAEvent event = new CAEvent(IAEvent.Type.ETranslationResults);
        event.put(IAEvent.TRANSLATION_RESULTS_SENTENCE, sentence);
        event.put(IAEvent.TRANSLATION_RESULTS_TRANSLATION, translatedString);
        return event;
    }

    private IAEvent buildErrorTranslationEvent(TAError error) {
        IAEvent event = new CAEvent(IAEvent.Type.ETranslationResults);
        event.put(IAEvent.ERROR, error);
        return event;
    }

    private String getTranslationFromJson(String jsonString)  {
        try{
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray translateArray = jsonObject.optJSONArray("text");
			if (translateArray.length() > 0)
				return translateArray.getString(0);
        } catch (JSONException e) {
			return null;
		}
		return null;
	}

    @Override
    public void start() {
        mEventDispatcher.subscribeListener(this, IAEvent.Type.EStringResponse);
    }
}
