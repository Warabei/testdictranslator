package me.testdicttranslator.managers;

import android.content.Context;
import android.os.Handler;
import me.testdicttranslator.managers.event.CAEventDispatcher;
import me.testdicttranslator.managers.event.IAEventDispatcher;

/**
 * Created by warabei on 11.02.2015.
 */
public class CAAppManager implements IAAppManager {

    private final Context mContext;
    private final IAEventDispatcher mEventDispatcher;
    private final IAHttpsManager mHttpManager;
    private final IATranslateManager mTranslateManager;
    private final IAExternalStorageManager mExternalStorageManager;
    private final IADictionaryManager mDictionaryManager;

    public CAAppManager(Context context) {
        mContext = context;
        mEventDispatcher = new CAEventDispatcher(new Handler());

        mHttpManager = new CAHttpsManager(mEventDispatcher);
        mTranslateManager = new CATranslateManager(mHttpManager, mEventDispatcher);
        mExternalStorageManager = new CAExternalStorageManager(context);
        mDictionaryManager = new CADictionaryManager(mEventDispatcher, mExternalStorageManager);
    }

    @Override
    public IAEventDispatcher getEventDispatcher() {
        return mEventDispatcher;
    }

    @Override
    public IATranslateManager getTranslateManager() {
        return mTranslateManager;
    }

    @Override
    public IADictionaryManager getDictionaryManager() {
        return mDictionaryManager;
    }

    @Override
    public void start() {
        mHttpManager.start();
        mTranslateManager.start();
        mExternalStorageManager.start();
        mDictionaryManager.start();
    }
}
