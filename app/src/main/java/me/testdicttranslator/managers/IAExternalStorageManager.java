package me.testdicttranslator.managers;

import android.content.Context;

import java.io.File;

/**
 * Created by warabei on 15.02.2015.
 */
public interface IAExternalStorageManager extends IAManager {
    boolean isExternalStorageAvailable();

    File getFile(String fileName);
}
