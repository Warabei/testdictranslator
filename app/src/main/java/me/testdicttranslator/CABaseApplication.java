package me.testdicttranslator;

import android.app.Application;
import me.testdicttranslator.managers.CAAppManager;
import me.testdicttranslator.managers.IAAppManager;

/**
 * Created by warabei on 11.02.2015.
 */
public class CABaseApplication extends Application {

    IAAppManager mAppManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppManager = new CAAppManager(this);
        mAppManager.start();
    }

    public IAAppManager getAppManager(){
        return mAppManager;
    }
}
